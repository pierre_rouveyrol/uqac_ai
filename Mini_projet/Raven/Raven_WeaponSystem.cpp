#include "Raven_WeaponSystem.h"
#include "armory/Weapon_RocketLauncher.h"
#include "armory/Weapon_RailGun.h"
#include "armory/Weapon_ShotGun.h"
#include "armory/Weapon_Blaster.h"
#include "Raven_Bot.h"
#include "misc/utils.h"
#include "lua/Raven_Scriptor.h"
#include "Raven_Game.h"
#include "Raven_UserOptions.h"
#include "2D/transformations.h"




//------------------------- ctor ----------------------------------------------
//-----------------------------------------------------------------------------
Raven_WeaponSystem::Raven_WeaponSystem(Raven_Bot* owner,
                                       double ReactionTime,
                                       double AimAccuracy,
                                       double AimPersistance):m_pOwner(owner),
                                                          m_dReactionTime(ReactionTime),
                                                          m_dAimAccuracy(AimAccuracy),
                                                          m_dAimPersistance(AimPersistance),
														  m_FuzzyModule(FuzzyModule::FuzzyModule())
{
  Initialize();
  pm_FuzzyModule = &m_FuzzyModule;
  m_dFuzzyAimAccuracy = 0;
  pm_dFuzzyAimAccuracy = &m_dFuzzyAimAccuracy;
  InitializeFuzzyModule();
}

//------------------------- dtor ----------------------------------------------
//-----------------------------------------------------------------------------
Raven_WeaponSystem::~Raven_WeaponSystem()
{
  for (unsigned int w=0; w<m_WeaponMap.size(); ++w)
  {
    delete m_WeaponMap[w];
  }
}

//------------------------------ Initialize -----------------------------------
//
//  initializes the weapons
//-----------------------------------------------------------------------------
void Raven_WeaponSystem::Initialize()
{
  //delete any existing weapons
  WeaponMap::iterator curW;
  for (curW = m_WeaponMap.begin(); curW != m_WeaponMap.end(); ++curW)
  {
    delete curW->second;
  }

  m_WeaponMap.clear();

  //set up the container
  m_pCurrentWeapon = new Blaster(m_pOwner);

  m_WeaponMap[type_blaster]         = m_pCurrentWeapon;
  m_WeaponMap[type_shotgun]         = 0;
  m_WeaponMap[type_rail_gun]        = 0;
  m_WeaponMap[type_rocket_launcher] = 0;
}

//-------------------------------- SelectWeapon -------------------------------
//
//-----------------------------------------------------------------------------
void Raven_WeaponSystem::SelectWeapon()
{ 
  //if a target is present use fuzzy logic to determine the most desirable 
  //weapon.
  if (m_pOwner->GetTargetSys()->isTargetPresent())
  {
    //calculate the distance to the target
    double DistToTarget = Vec2DDistance(m_pOwner->Pos(), m_pOwner->GetTargetSys()->GetTarget()->Pos());

    //for each weapon in the inventory calculate its desirability given the 
    //current situation. The most desirable weapon is selected
    double BestSoFar = MinDouble;

    WeaponMap::const_iterator curWeap;
    for (curWeap=m_WeaponMap.begin(); curWeap != m_WeaponMap.end(); ++curWeap)
    {
      //grab the desirability of this weapon (desirability is based upon
      //distance to target and ammo remaining)
      if (curWeap->second)
      {
        double score = curWeap->second->GetDesirability(DistToTarget);

        //if it is the most desirable so far select it
        if (score > BestSoFar)
        {
          BestSoFar = score;

          //place the weapon in the bot's hand.
          m_pCurrentWeapon = curWeap->second;
        }
      }
    }
  }

  else
  {
    m_pCurrentWeapon = m_WeaponMap[type_blaster];
  }
}

//--------------------  AddWeapon ------------------------------------------
//
//  this is called by a weapon affector and will add a weapon of the specified
//  type to the bot's inventory.
//
//  if the bot already has a weapon of this type then only the ammo is added
//-----------------------------------------------------------------------------
void  Raven_WeaponSystem::AddWeapon(unsigned int weapon_type)
{
  //create an instance of this weapon
  Raven_Weapon* w = 0;

  switch(weapon_type)
  {
  case type_rail_gun:

    w = new RailGun(m_pOwner); break;

  case type_shotgun:

    w = new ShotGun(m_pOwner); break;

  case type_rocket_launcher:

    w = new RocketLauncher(m_pOwner); break;

  }//end switch
  

  //if the bot already holds a weapon of this type, just add its ammo
  Raven_Weapon* present = GetWeaponFromInventory(weapon_type);

  if (present)
  {
    present->IncrementRounds(w->NumRoundsRemaining());

    delete w;
  }
  
  //if not already holding, add to inventory
  else
  {
    m_WeaponMap[weapon_type] = w;
  }
}


//------------------------- GetWeaponFromInventory -------------------------------
//
//  returns a pointer to any matching weapon.
//
//  returns a null pointer if the weapon is not present
//-----------------------------------------------------------------------------
Raven_Weapon* Raven_WeaponSystem::GetWeaponFromInventory(int weapon_type)
{
  return m_WeaponMap[weapon_type];
}

//----------------------- ChangeWeapon ----------------------------------------
void Raven_WeaponSystem::ChangeWeapon(unsigned int type)
{
  Raven_Weapon* w = GetWeaponFromInventory(type);

  if (w) m_pCurrentWeapon = w;
}

//--------------------------- TakeAimAndShoot ---------------------------------
//
//  this method aims the bots current weapon at the target (if there is a
//  target) and, if aimed correctly, fires a round
//-----------------------------------------------------------------------------
void Raven_WeaponSystem::TakeAimAndShoot()const
{
  //aim the weapon only if the current target is shootable or if it has only
  //very recently gone out of view (this latter condition is to ensure the 
  //weapon is aimed at the target even if it temporarily dodges behind a wall
  //or other cover)
  if (m_pOwner->GetTargetSys()->isTargetShootable() ||
      (m_pOwner->GetTargetSys()->GetTimeTargetHasBeenOutOfView() < 
       m_dAimPersistance) )
  {
    //the position the weapon will be aimed at
    Vector2D AimingPos = m_pOwner->GetTargetBot()->Pos();
    
    //if the current weapon is not an instant hit type gun the target position
    //must be adjusted to take into account the predicted movement of the 
    //target
    if (GetCurrentWeapon()->GetType() == type_rocket_launcher ||
        GetCurrentWeapon()->GetType() == type_blaster)
    {
      AimingPos = PredictFuturePositionOfTarget();

      //if the weapon is aimed correctly, there is line of sight between the
      //bot and the aiming position and it has been in view for a period longer
      //than the bot's reaction time, shoot the weapon
      if ( m_pOwner->RotateFacingTowardPosition(AimingPos) &&
           (m_pOwner->GetTargetSys()->GetTimeTargetHasBeenVisible() >
            m_dReactionTime) &&
           m_pOwner->hasLOSto(AimingPos) )
      {
        AddFuzzyNoiseToAim(AimingPos);

        GetCurrentWeapon()->ShootAt(AimingPos);
      }
    }

    //no need to predict movement, aim directly at target
    else
    {
      //if the weapon is aimed correctly and it has been in view for a period
      //longer than the bot's reaction time, shoot the weapon
      if ( m_pOwner->RotateFacingTowardPosition(AimingPos) &&
           (m_pOwner->GetTargetSys()->GetTimeTargetHasBeenVisible() >
            m_dReactionTime) )
      {
        AddFuzzyNoiseToAim(AimingPos);
        
        GetCurrentWeapon()->ShootAt(AimingPos);
      }
    }

  }
  
  //no target to shoot at so rotate facing to be parallel with the bot's
  //heading direction
  else
  {
    m_pOwner->RotateFacingTowardPosition(m_pOwner->Pos()+ m_pOwner->Heading());
  }
}

//---------------------------- AddNoiseToAim ----------------------------------
//
//  adds a random deviation to the firing angle not greater than m_dAimAccuracy 
//  rads
//-----------------------------------------------------------------------------
void Raven_WeaponSystem::AddNoiseToAim(Vector2D& AimingPos)const
{
  Vector2D toPos = AimingPos - m_pOwner->Pos();

  Vec2DRotateAroundOrigin(toPos, RandInRange(-m_dAimAccuracy, m_dAimAccuracy));

  AimingPos = toPos + m_pOwner->Pos();
}

//---------------------------- AddFuzzyNoiseToAim ----------------------------------
//
//  adds a fuzzy deviation to the firing angle not greater than m_dAimAccuracy 
//  rads based on multiple factors
//-----------------------------------------------------------------------------
void Raven_WeaponSystem::AddFuzzyNoiseToAim(Vector2D& AimingPos)const
{
  Vector2D toPos = AimingPos - m_pOwner->Pos();
  double DistToTarget = Vec2DDistance(m_pOwner->Pos(), m_pOwner->GetTargetSys()->GetTarget()->Pos());
  
  pm_FuzzyModule->Fuzzify("DistanceToTarget", DistToTarget);
  pm_FuzzyModule->Fuzzify("Velocity", (double)m_pOwner->GetTargetSys()->GetTarget()->Speed());
  pm_FuzzyModule->Fuzzify("TimeTargetHasBeenVisible", (double)m_pOwner->GetTargetSys()->GetTimeTargetHasBeenVisible());
  *pm_dFuzzyAimAccuracy = pm_FuzzyModule->DeFuzzify("AccuracyLevel", FuzzyModule::max_av);

  Vec2DRotateAroundOrigin(toPos, RandInRange(-m_dFuzzyAimAccuracy, m_dFuzzyAimAccuracy));

  AimingPos = toPos + m_pOwner->Pos();
}

//--------------------------- InitializeFuzzyModule ---------------------------
//
//  set up some fuzzy variables and rules that adjust m_dAimAccuracy between 0 and pi/4
//-----------------------------------------------------------------------------
void Raven_WeaponSystem::InitializeFuzzyModule()
{  
  FuzzyVariable& DistanceToTarget = m_FuzzyModule.CreateFLV("DistanceToTarget");
  FzSet& Target_Close = DistanceToTarget.AddLeftShoulderSet("Target_Close", 0, 25, 150);
  FzSet& Target_Medium = DistanceToTarget.AddTriangularSet("Target_Medium", 25, 150, 300);
  FzSet& Target_Far = DistanceToTarget.AddRightShoulderSet("Target_Far", 150, 300, 1000);


  FuzzyVariable& Velocity = m_FuzzyModule.CreateFLV("Velocity");
  FzSet& Slow = Velocity.AddLeftShoulderSet("Slow", 0, 25, 50);
  FzSet& Average = Velocity.AddTriangularSet("Average", 25, 50, 75);
  FzSet& Fast = Velocity.AddRightShoulderSet("Fast", 50, 75, 100);


  FuzzyVariable& TimeTargetHasBeenVisible = m_FuzzyModule.CreateFLV("TimeTargetHasBeenVisible");
  FzSet& Long = TimeTargetHasBeenVisible.AddRightShoulderSet("Long", 30, 60, 100);
  FzSet& Enough = TimeTargetHasBeenVisible.AddTriangularSet("Enough", 0, 30, 60);
  FzSet& Little = TimeTargetHasBeenVisible.AddTriangularSet("Little", 0, 0, 30);


  FuzzyVariable& AccuracyLevel = m_FuzzyModule.CreateFLV("AccuracyLevel");
  FzSet& Parkinson = AccuracyLevel.AddRightShoulderSet("Parkinson", 0.8, 1.2, 1.6);
  FzSet& Shaky = AccuracyLevel.AddTriangularSet("Shaky", 0.4, 0.8, 1.2);
  FzSet& Okay = AccuracyLevel.AddTriangularSet("Okay", 0.08, 0.2, 0.6);
  FzSet& Nice = AccuracyLevel.AddTriangularSet("Nice", 0.04, 0.06, 0.1);
  FzSet& Epic = AccuracyLevel.AddLeftShoulderSet("Epic", 0, 0, 0.04);

  m_FuzzyModule.AddRule(FzAND(Target_Close, Slow, Long), Epic);
  m_FuzzyModule.AddRule(FzAND(Target_Close, Slow, Enough), Epic);
  m_FuzzyModule.AddRule(FzAND(Target_Close, Slow, Little), Epic);

  m_FuzzyModule.AddRule(FzAND(Target_Close, Average, Long), Nice);
  m_FuzzyModule.AddRule(FzAND(Target_Close, Average, Enough), Nice);
  m_FuzzyModule.AddRule(FzAND(Target_Close, Average, Little), Nice);

  m_FuzzyModule.AddRule(FzAND(Target_Close, Fast, Long), Nice);
  m_FuzzyModule.AddRule(FzAND(Target_Close, Fast, Enough), Okay);
  m_FuzzyModule.AddRule(FzAND(Target_Close, Fast, Little), Okay);


  m_FuzzyModule.AddRule(FzAND(Target_Medium, Slow, Long), Nice);
  m_FuzzyModule.AddRule(FzAND(Target_Medium, Slow, Enough), Nice);
  m_FuzzyModule.AddRule(FzAND(Target_Medium, Slow, Little), Okay);

  m_FuzzyModule.AddRule(FzAND(Target_Medium, Average, Long), Okay);
  m_FuzzyModule.AddRule(FzAND(Target_Medium, Average, Enough), Okay);
  m_FuzzyModule.AddRule(FzAND(Target_Medium, Average, Little), Shaky);

  m_FuzzyModule.AddRule(FzAND(Target_Medium, Fast, Long), Okay);
  m_FuzzyModule.AddRule(FzAND(Target_Medium, Fast, Enough), Shaky);
  m_FuzzyModule.AddRule(FzAND(Target_Medium, Fast, Little), Shaky);


  m_FuzzyModule.AddRule(FzAND(Target_Far, Slow, Long), Okay);
  m_FuzzyModule.AddRule(FzAND(Target_Far, Slow, Enough), Okay);
  m_FuzzyModule.AddRule(FzAND(Target_Far, Slow, Little), Shaky);

  m_FuzzyModule.AddRule(FzAND(Target_Far, Average, Long), Shaky);
  m_FuzzyModule.AddRule(FzAND(Target_Far, Average, Enough), Shaky);
  m_FuzzyModule.AddRule(FzAND(Target_Far, Average, Little), Parkinson);

  m_FuzzyModule.AddRule(FzAND(Target_Far, Fast, Long), Parkinson);
  m_FuzzyModule.AddRule(FzAND(Target_Far, Fast, Enough), Parkinson);
  m_FuzzyModule.AddRule(FzAND(Target_Far, Fast, Little), Parkinson);
}


//-------------------------- PredictFuturePositionOfTarget --------------------
//
//  predicts where the target will be located in the time it takes for a
//  projectile to reach it. This uses a similar logic to the Pursuit steering
//  behavior.
//-----------------------------------------------------------------------------
Vector2D Raven_WeaponSystem::PredictFuturePositionOfTarget()const
{
  double MaxSpeed = GetCurrentWeapon()->GetMaxProjectileSpeed();
  
  //if the target is ahead and facing the agent shoot at its current pos
  Vector2D ToEnemy = m_pOwner->GetTargetBot()->Pos() - m_pOwner->Pos();
 
  //the lookahead time is proportional to the distance between the enemy
  //and the pursuer; and is inversely proportional to the sum of the
  //agent's velocities
  double LookAheadTime = ToEnemy.Length() / 
                        (MaxSpeed + m_pOwner->GetTargetBot()->MaxSpeed());
  
  //return the predicted future position of the enemy
  return m_pOwner->GetTargetBot()->Pos() + 
         m_pOwner->GetTargetBot()->Velocity() * LookAheadTime;
}


//------------------ GetAmmoRemainingForWeapon --------------------------------
//
//  returns the amount of ammo remaining for the specified weapon. Return zero
//  if the weapon is not present
//-----------------------------------------------------------------------------
int Raven_WeaponSystem::GetAmmoRemainingForWeapon(unsigned int weapon_type)
{
  if (m_WeaponMap[weapon_type])
  {
    return m_WeaponMap[weapon_type]->NumRoundsRemaining();
  }

  return 0;
}

//---------------------------- ShootAt ----------------------------------------
//
//  shoots the current weapon at the given position
//-----------------------------------------------------------------------------
void Raven_WeaponSystem::ShootAt(Vector2D pos)const
{
  GetCurrentWeapon()->ShootAt(pos);
}

//-------------------------- RenderCurrentWeapon ------------------------------
//-----------------------------------------------------------------------------
void Raven_WeaponSystem::RenderCurrentWeapon()const
{
  GetCurrentWeapon()->Render();
}

void Raven_WeaponSystem::RenderDesirabilities()const
{
  Vector2D p = m_pOwner->Pos();

  int num = 0;
  
  WeaponMap::const_iterator curWeap;
  for (curWeap=m_WeaponMap.begin(); curWeap != m_WeaponMap.end(); ++curWeap)
  {
    if (curWeap->second) num++;
  }

  int offset = 15 * num;

    for (curWeap=m_WeaponMap.begin(); curWeap != m_WeaponMap.end(); ++curWeap)
    {
      if (curWeap->second)
      {
        double score = curWeap->second->GetLastDesirabilityScore();
        std::string type = GetNameOfType(curWeap->second->GetType());

        gdi->TextAtPos(p.x+10.0, p.y-offset, ttos(score) + " " + type);

        offset+=15;
      }
    }
}
