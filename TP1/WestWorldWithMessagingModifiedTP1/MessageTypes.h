#ifndef MESSAGE_TYPES
#define MESSAGE_TYPES

#include <string>

enum message_type
{
  Msg_HiHoneyImHome,
  Msg_StewReady,
  Msg_HelloSaloon,
  Msg_HelloFriend,
  Msg_GimmeWhiskey,
};


inline std::string MsgToStr(int msg)
{
	switch (msg)
	{
	case Msg_HiHoneyImHome:
		return "HiHoneyImHome"; 

	case Msg_StewReady:
		return "StewReady";

	case Msg_HelloSaloon:
		return "Hello guys! Boy! Get me a glass o' strong booze!";

	case Msg_HelloFriend:
		return "Hello Bob ol' friend, is Gold searching goin' fine?";

	case Msg_GimmeWhiskey:
		return "Hic! Hey Bob ol' mate! Buy me a drink!";

	default:
		return "Not recognized!";
	}
}

#endif