#ifndef DRUNKGUY_H
#define DRUNKGUY_H
//------------------------------------------------------------------------
//
//  Name:   DrunkGuy.h
//
//  Desc:   A class defining a guy that is drunk most of the time.
//
//  Author: Pierre Rouveyrol 2012
//
//------------------------------------------------------------------------
#include <string>
#include <cassert>
#include <iostream>

#include "BaseGameEntity.h"
#include "Locations.h"
#include "misc/ConsoleUtils.h"
#include "DrunkGuyOwnedStates.h"
#include "fsm/StateMachine.h"

struct Telegram;

//below this value the drunk guy is (somewhat) sober
const int DrunknessThreshold = 5;
//above this value a hobo is sleepy
const int FatigueThreshold = 5;


class DrunkGuy : public BaseGameEntity
{
private:

  //an instance of the state machine class
  StateMachine<DrunkGuy>*  m_pStateMachine;
  location_type         m_Location;

  //the higher the value, the more drunk the guy
  int                   m_iDrunkennessLevel;
  int					m_iFatigueLevel;


public:

  DrunkGuy(int id):	m_Location(saloon),
					m_iDrunkennessLevel(0),
					m_iFatigueLevel(0),
                    BaseGameEntity(id)
                               
  {
    //set up state machine
    m_pStateMachine = new StateMachine<DrunkGuy>(this);
    
    m_pStateMachine->SetCurrentState(Sober::Instance());

    /* NOTE, A GLOBAL STATE HAS NOT BEEN IMPLEMENTED FOR THE DRUNKGUY */
  }

  ~DrunkGuy(){delete m_pStateMachine;}

  //this must be implemented
  void Update();

  //so must this
  virtual bool  HandleMessage(const Telegram& msg);

  
  StateMachine<DrunkGuy>* GetFSM()const{return m_pStateMachine;}


  
  //-------------------------------------------------------------accessors
  location_type Location()const{return m_Location;}
  void          ChangeLocation(location_type loc){m_Location=loc;}

  bool          Fatigued()const;
  bool          Rested()const;

  void          BuyAndDrinkAWhiskey(){m_iDrunkennessLevel += 1;}

  void          DecreaseFatigue(){m_iFatigueLevel -= 1;}
  void          IncreaseFatigue(){m_iFatigueLevel += 1;}

  void			SetSober(){m_iDrunkennessLevel = 0; m_iFatigueLevel = 0;}

  bool			Sober()const;
  bool          Drunken()const;
};



#endif
