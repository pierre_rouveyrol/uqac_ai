#ifndef DRUNKGUY_OWNED_STATES_H
#define DRUNKGUY_OWNED_STATES_H
//------------------------------------------------------------------------
//
//  Name:   DrunkGuyOwnedStates.h
//
//  Desc:   All the states that can be assigned to the DrunkGuy class.
//          Note that a global state has not been implemented.
//
//  Author: Pierre Rouveyrol 2012
//
//------------------------------------------------------------------------
#include "fsm/State.h"
#include <string>
using namespace std;

class DrunkGuy;
struct Telegram;

//------------------------------------------------------------------------
//Drunkguy changes location to the saloon and keeps buying Whiskey until
//he is drunk.
//------------------------------------------------------------------------
class Sober : public State<DrunkGuy>
{
private:
  
  Sober(){}

  //copy ctor and assignment should be private
  Sober(const Sober&);
  Sober& operator=(const Sober&);
 
public:

  //this is a singleton
  static Sober* Instance();

  virtual void Enter(DrunkGuy* drunkGuy);

  virtual void Execute(DrunkGuy* drunkGuy);

  virtual void Exit(DrunkGuy* drunkGuy);

  virtual bool OnMessage(DrunkGuy* agent, const Telegram& msg);
};

//------------------------------------------------------------------------
//Drunkguy is drunk. He does lots of nonsense, draining his energy,
//once he's tired, he goes to find some place to sleep.
//------------------------------------------------------------------------
class Drunk : public State<DrunkGuy>
{
private:
	Drunk()
	{
			DrunkenBlabber[0] = "Ugh, I is drunk";
			DrunkenBlabber[1] = "*Trying to open a non-existent door*";
			DrunkenBlabber[2] = "*Getting friendly with a dog*";
			DrunkenBlabber[3] = "*Harrassing a poor lady*";
			DrunkenBlabber[4] = "*Stumbling upon nothing*";
	}

  
  //copy ctor and assignment should be private
  Drunk(const Drunk&);
  Drunk& operator=(const Drunk&);
  string DrunkenBlabber[5];
 
public:
	
  //this is a singleton
  static Drunk* Instance();

  virtual void Enter(DrunkGuy* drunkGuy);

  virtual void Execute(DrunkGuy* drunkGuy);

  virtual void Exit(DrunkGuy* drunkGuy);

  virtual bool OnMessage(DrunkGuy* agent, const Telegram& msg);
};

//------------------------------------------------------------------------
//Drunkguy sleeps in a stable, he gets harrassed by a damn' horse, once he's
//rested enough, he goes back to his favorite activity.
//------------------------------------------------------------------------
class Hangover : public State<DrunkGuy>
{
private:
  
  Hangover(){}

  //copy ctor and assignment should be private
  Hangover(const Hangover&);
  Hangover& operator=(const Hangover&);
 
public:

  //this is a singleton
  static Hangover* Instance();

  virtual void Enter(DrunkGuy* drunkGuy);

  virtual void Execute(DrunkGuy* drunkGuy);

  virtual void Exit(DrunkGuy* drunkGuy);

  virtual bool OnMessage(DrunkGuy* agent, const Telegram& msg);
};

//------------------------------------------------------------------------
//Drunkguy has provoked Bob, but he doesn't know that ol' bob has a pickaxe in his bag,
//that's why our drunken friend gets kicked, and then goes to bed.
//------------------------------------------------------------------------
class GettingKicked : public State<DrunkGuy>
{
private:
  
  GettingKicked(){}

  //copy ctor and assignment should be private
  GettingKicked(const GettingKicked&);
  GettingKicked& operator=(const GettingKicked&);
 
public:

  //this is a singleton
  static GettingKicked* Instance();

  virtual void Enter(DrunkGuy* drunkGuy);

  virtual void Execute(DrunkGuy* drunkGuy);

  virtual void Exit(DrunkGuy* drunkGuy);

  virtual bool OnMessage(DrunkGuy* agent, const Telegram& msg);
};

#endif