#include "DrunkGuy.h"

bool DrunkGuy::HandleMessage(const Telegram& msg)
{
  return m_pStateMachine->HandleMessage(msg);
}

void DrunkGuy::Update()
{
  SetTextColor(FOREGROUND_BLUE| FOREGROUND_INTENSITY);
  
  m_pStateMachine->Update();
}

bool DrunkGuy::Sober()const
{
	return (m_iDrunkennessLevel==0);
}
bool DrunkGuy::Drunken()const
{
	return (m_iDrunkennessLevel >= DrunknessThreshold);
}

bool DrunkGuy::Fatigued()const
{
	return (m_iFatigueLevel > FatigueThreshold);
}

bool DrunkGuy::Rested()const
{
	return (m_iFatigueLevel == 0);
}

