#include "DrunkGuyOwnedStates.h"
#include "fsm/State.h"
#include "DrunkGuy.h"
#include "Locations.h"
#include "messaging/Telegram.h"
#include "MessageDispatcher.h"
#include "MessageTypes.h"
#include "Time/CrudeTimer.h"
#include "EntityNames.h"

#include <iostream>
using std::cout;


#ifdef TEXTOUTPUT
#include <fstream>
extern std::ofstream os;
#define cout os
#endif


//------------------------------------------------------------------------Drunk
Drunk* Drunk::Instance()
{
  static Drunk instance;

  return &instance;
}

void Drunk::Enter(DrunkGuy* pDrunkGuy)
{
  if (pDrunkGuy->Location() != saloon)
  {    
    pDrunkGuy->ChangeLocation(saloon);

    cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) << ": " << "UUUgh! That was one too much...";
  }
}

void Drunk::Execute(DrunkGuy* pDrunkGuy)
{
	if(!pDrunkGuy->Fatigued())
	{
		pDrunkGuy->BuyAndDrinkAWhiskey();
		cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) << ": " << this->DrunkenBlabber[rand() % 5];
		pDrunkGuy->IncreaseFatigue();
	}
	else
	{
	  pDrunkGuy->GetFSM()->ChangeState(Hangover::Instance());
	}
}

void Drunk::Exit(DrunkGuy* pDrunkGuy)
{ 
  cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) << ": " << "Uhh, need to sleep a little, where's my room?";
}

bool Drunk::OnMessage(DrunkGuy* pDrunkGuy, const Telegram& msg)
{
   SetTextColor(BACKGROUND_RED|FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);

   switch(msg.Msg)
   {
   case Msg_HelloSaloon:

     cout << "\nMessage handled by " << GetNameOfEntity(pDrunkGuy->ID()) 
     << " at time: " << Clock->GetCurrentTime();

     SetTextColor(FOREGROUND_BLUE|FOREGROUND_INTENSITY);

     cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) 
          << ": Hey Bob, you comin' just fine, buy me a drink so I don't need to kick you!";

	         //let guys know i'm here
	Dispatch->DispatchMessage(SEND_MSG_IMMEDIATELY,
							  pDrunkGuy->ID(),
							  ent_Miner_Bob,
							  Msg_GimmeWhiskey,
							  NO_ADDITIONAL_INFO);

	pDrunkGuy->GetFSM()->ChangeState(GettingKicked::Instance());
      
     return true;

   }//end switch

   return false; //send message to global message handler
}


//------------------------------------------------------------------------Sober
Sober* Sober::Instance()
{
  static Sober instance;

  return &instance;
}

void Sober::Enter(DrunkGuy* pDrunkGuy)
{
  if (pDrunkGuy->Location() != saloon)
  {    
    pDrunkGuy->ChangeLocation(saloon);

    cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) << ": " << "Boy, ah sure is too sober! Getting a drink";
  }
}

void Sober::Execute(DrunkGuy* pDrunkGuy)
{
	if(!pDrunkGuy->Drunken())
	{
		pDrunkGuy->BuyAndDrinkAWhiskey();
		cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) << ": " << "That's a good whiskey, please another one!";
	}
	else
	{
	  pDrunkGuy->GetFSM()->ChangeState(Drunk::Instance());
	}
}

void Sober::Exit(DrunkGuy* pDrunkGuy)
{ 
  cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) << ": " << "Feeling tipsy...";
}

bool Sober::OnMessage(DrunkGuy* pDrunkGuy, const Telegram& msg)
{
   SetTextColor(BACKGROUND_RED|FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);

   switch(msg.Msg)
   {
   case Msg_HelloSaloon:

     cout << "\nMessage handled by " << GetNameOfEntity(pDrunkGuy->ID()) 
     << " at time: " << Clock->GetCurrentTime();

     SetTextColor(FOREGROUND_BLUE|FOREGROUND_INTENSITY);

     cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) 
          << ": Hey Bob, ol' friend, how are ye?";

	         //let guys know i'm here
	Dispatch->DispatchMessage(SEND_MSG_IMMEDIATELY,
							  pDrunkGuy->ID(),
							  ent_Miner_Bob,
							  Msg_HelloFriend,
							  NO_ADDITIONAL_INFO);
      
     return true;

   }//end switch

   return false; //send message to global message handler
}


//------------------------------------------------------------------------Hangover
Hangover* Hangover::Instance()
{
  static Hangover instance;

  return &instance;
}

void Hangover::Enter(DrunkGuy* pDrunkGuy)
{
  if (pDrunkGuy->Location() != stable)
  {    
    pDrunkGuy->ChangeLocation(stable);

    cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) << ": " << "Ah, here's a hay stack in a stable! Just what I needed!";
  }
}

void Hangover::Execute(DrunkGuy* pDrunkGuy)
{
	if(!pDrunkGuy->Rested())
	{
		cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) << ": " << "ZZzZz... No hoers, leave me alone";
		pDrunkGuy->DecreaseFatigue();
		pDrunkGuy->DecreaseFatigue();
	}
	else
	{
		pDrunkGuy->SetSober();
		pDrunkGuy->GetFSM()->ChangeState(Sober::Instance());
	}
}

void Hangover::Exit(DrunkGuy* pDrunkGuy)
{ 
  cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) << ": " << "Ahh, feelin' good, time to go waste myself again!";
}

bool Hangover::OnMessage(DrunkGuy* pDrunkGuy, const Telegram& msg)
{
  //send msg to global message handler
  return false;
}


//------------------------------------------------------------------------GettingKicked
GettingKicked* GettingKicked::Instance()
{
  static GettingKicked instance;
  return &instance;
}

void GettingKicked::Enter(DrunkGuy* pDrunkGuy)
{
  if (pDrunkGuy->Location() != saloon)
  {    
    pDrunkGuy->ChangeLocation(saloon);
    cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) << ": " << "Ahh you damn' Bob, you don't want to buy me some booze with all the gold ye earned, a'll kick you the ol' way!";
  }
}

void GettingKicked::Execute(DrunkGuy* pDrunkGuy)
{
		cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) << ": " << "Aoutch! No! Ye have yar mighty pickaxe!!";
		pDrunkGuy->IncreaseFatigue();
		pDrunkGuy->IncreaseFatigue();
		pDrunkGuy->GetFSM()->ChangeState(Hangover::Instance());
}

void GettingKicked::Exit(DrunkGuy* pDrunkGuy)
{ 
  cout << "\n" << GetNameOfEntity(pDrunkGuy->ID()) << ": " << "Aoutch, it hurts! Gotta find some place to lay down and recover.";
}

bool GettingKicked::OnMessage(DrunkGuy* pDrunkGuy, const Telegram& msg)
{
  //send msg to global message handler
  return false;
}