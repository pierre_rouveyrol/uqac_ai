#include "Vehicle.h"
#include "Follower.h"
#include "ParamLoader.h"
#include "SteeringBehaviors.h"

Follower::Follower(GameWorld* GameWorld, Vector2D SpawnPos, Vehicle* toFollow):Vehicle(GameWorld,SpawnPos,RandFloat()*TwoPi,Vector2D(0,0),Prm.VehicleMass/10,Prm.MaxSteeringForce,Prm.MaxSpeed,Prm.MaxTurnRatePerSecond,Prm.VehicleScale)
{
	this->Steering()->OffsetPursuitOn(toFollow,Vector2D(-25, 0));
	this->Steering()->SeparationOn();
	this->Steering()->WallAvoidanceOn();
}