#ifndef FOLLOWER_H
#define FOLLOWER_H
//------------------------------------------------------------------------
//
//  Name:   Leader.h
//
//  Desc:   Definition of a specific vehicle follows in line a leader, and avoids collision with every other agent
//
//  Author: Pierre Rouveyrol 2012
//
//------------------------------------------------------------------------
#include "Vehicle.h"

class Follower : public Vehicle
{

private:



public:

  Follower(GameWorld* GameWorld, Vector2D SpawnPos, Vehicle* toFollow);

  ~Follower(){};
};



#endif