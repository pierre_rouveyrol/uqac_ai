#include "Vehicle.h"
#include "Leader.h"
#include "ParamLoader.h"
#include "SteeringBehaviors.h"

Leader::Leader(GameWorld* GameWorld, Vector2D SpawnPos):Vehicle(GameWorld,SpawnPos,RandFloat()*TwoPi,Vector2D(0,0),Prm.VehicleMass,Prm.MaxSteeringForce,Prm.MaxSpeed,Prm.MaxTurnRatePerSecond,Prm.VehicleScale*2)
{
	this->Steering()->WanderOn();
	this->Steering()->SeparationOn();
	this->Steering()->WallAvoidanceOn();
	this->SetMaxSpeed(100);
	this->SetScale(Vector2D(10, 10));
}