#ifndef LEADER_H
#define LEADER_H
//------------------------------------------------------------------------
//
//  Name:   Leader.h
//
//  Desc:   Definition of a specific vehicle that only wanders
//
//  Author: Pierre Rouveyrol 2012
//
//------------------------------------------------------------------------
#include "Vehicle.h"

class Leader : public Vehicle
{

private:



public:

  Leader(GameWorld* GameWorld,Vector2D SpawnPos);

  ~Leader(){};
};



#endif